use std::env;

/// Struct that stores the configuration for the random token generation.
///
/// The 'length' property indicates the number of hexadecimal pairs the generated token will have. If None is provided, the default length (32) will be used.
///
/// # Examples
/// ```
/// use randtoken::config::Config;
/// let my_config_1 = Config {length: Some(45)};
/// let my_config_2 = Config {length: None};
///```
pub struct Config {
    pub length: Option<u128>,
}

impl Config {
    /// Constructs a new Config struct using the provided Args.
    ///
    /// The first argument will be parsed and used for the 'length' property, if possible. If this argument is not a valid number, an Err will be returned.
    ///
    /// # Examples
    /// ```
    /// use std::env;
    /// use randtoken::config::Config;
    /// let config = Config::new(env::args()).unwrap_or_else(|err| {
    ///    panic!("Problem parsing arguments: {}", err);
    /// });
    /// ```
    pub fn new(mut args: env::Args) -> Result<Config, String> {
        args.next();
        if let Some(arg) = args.next() {
            match arg.parse::<u128>() {
                Ok(length) => Ok(Config {
                    length: Some(length),
                }),
                Err(_) => Err(format!(
                    "Length argument must be a valid number, found: {}",
                    arg
                )),
            }
        } else {
            Ok(Config { length: None })
        }
    }
}
