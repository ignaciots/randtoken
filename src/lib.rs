use rand::distributions::Uniform;
use rand::Rng;
use std::convert::TryInto;
use std::error::Error;
use std::fmt::Write;

pub mod config;

const DEFAULT_CONFIG_LENGTH: u128 = 32;

/// Prints a random hexadecimal string token using the given configuration to the standard output.
/// # Examples
/// ```
/// use randtoken::run;
/// use randtoken::config::Config;
/// run(Config {length: Some(5)});
/// ```
/// This example will return a random hexadecimal string with 5 hexadecimal pairs. For example, it could return `47ce3395d0`.
pub fn run(config: config::Config) -> Result<(), Box<dyn Error>> {
    let token = randtoken(config.length.unwrap_or(DEFAULT_CONFIG_LENGTH))?;
    println!("{}", token);
    Ok(())
}

fn randtoken(length: u128) -> Result<String, Box<dyn Error>> {
    let length: usize = match length.try_into() {
        Ok(length) => length,
        Err(_) => {
            return Err(format!("Number {} is too long.", length).into());
        }
    };

    Ok(rand::thread_rng()
        .sample_iter(Uniform::new(0, 255))
        .take(length)
        .fold(String::new(), |mut acc, val| {
            let _ = write!(acc, "{val:02x}");
            acc
        }))
}

#[cfg(test)]
mod tests {
    use super::randtoken;
    use std::error::Error;

    const HEX_CHARS: [char; 16] = [
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f',
    ];

    #[test]
    fn is_correct_length() -> Result<(), Box<dyn Error>> {
        for i in 0..32 {
            assert_eq!(randtoken(i)?.len(), i as usize * 2);
        }
        Ok(())
    }

    #[test]
    fn is_correct_content() -> Result<(), Box<dyn Error>> {
        for i in 0..32 {
            assert!(randtoken(i)?
                .chars()
                .into_iter()
                .all(|c| HEX_CHARS.contains(&c)));
        }
        Ok(())
    }
}
