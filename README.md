# randtoken
Random token generator.

## Usage

```
randtoken [length]
```

This program generates random hexadecimal string tokens with a specified pair length.

If no length is specified, the default length (**32**) will be used.

### Example output

For the following command:
```
randtoken 12
```
a possible output value would be:
```
9db7533451b6eca08d3d45f3
```
