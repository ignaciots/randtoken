# randtoken
Generador de números hexadecimales aleatorios.

## Uso

```
randtoken [length]
```

Este programa general un número hexadecimal aleatorio según el tamaño especificado. Cada número de tamaño equivale a dos caracteres hexadecimales.

Si no se especifica el tamaño, se usará el predeterminado (32).

### Ejemplo

Para la siguiente orden:
```
randtoken 12
```
un posible valor de salida sería:
```
9db7533451b6eca08d3d45f3
```
